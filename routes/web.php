<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route:: get('/dashboard', function(){
	return view('dashboard.dashboard');
});

// CMS Get Pages

Route::get('/dashboard/pages/home', function(){
	$homedata = DB::table('settings')->where('optname', "homepage")->get();
	$opts = array( 'view' => 'pages', );
	return view('pages.home', compact('homedata', 'opts'));
});

Route::get('/dashboard/pages/about', function(){
	$homedata = DB::table('settings')->where('optname', "aboutpage")->get();
	$opts = array( 'view' => 'pages', );
	return view('pages.about', compact('homedata', 'opts'));
});

Route::get('/dashboard/pages/pricing', function(){
	$opts = array( 'view' => 'pages', );
	return view('pages.pricing', compact('opts'));
});

Route::get('/dashboard/pages/contact', function(){
	$homedata = DB::table('settings')->where('optname', "contactpage")->get();
	$opts = array( 'view' => 'pages', );
	return view('pages.contact', compact('homedata','opts'));
});

Route::get('/dashboard/pages/privacy', function(){
	$homedata = DB::table('settings')->where('optname', "contactpage")->get();
	$opts = array( 'view' => 'pages', );
	return view('pages.privacy', compact('homedata','opts'));
});

Route::get('/dashboard/pages/terms', function(){
	$homedata = DB::table('settings')->where('optname', "contactpage")->get();
	$opts = array( 'view' => 'pages', );
	return view('pages.terms', compact('homedata', 'opts'));
});

Route::get('/dashboard/vendors', function(){
	$listofvendors = DB::table('vendor')->get();
	$opts = array( 'view' => 'vendors', );
	return view('pages.vendors', compact('listofvendors', 'opts'));
});

Route::get('/dashboard/users', function(){
	$listofusers = DB::table('nfpusers')->get();
	$opts = array( 'view' => 'users', );
	return view('pages.users', compact('listofusers', 'opts'));
});

Route::get('/dashboard/users/{uid}', function($uid){
	$profile = DB::table('nfpusers')->where('uid', $uid)->get();
	$opts = array( 'view' => 'users', );
	return view('users.viewusers', compact('profile', 'opts'));
});

Route::get('/dashboard/vendors/{vid}/{action}', 'dashboardController@changesVendorStatus');

// CMS Post Pages

// Save CMS pages
Route::post('/dashboard/pages/{saveto}', 'dashboardController@savePage');	

// Test Form Submition before Saving on actual DB
Route::post('/dashboard/pages/test/{saveto}', 'dashboardController@saveTest');

// Save CMS General settings
Route::post('/dashboard/settings', 'dashboardController@saveSettings');	


// CMS Settings
Route::get('/dashboard/settings', function(){
	$homedata = DB::table('settings')->where('optname', "settings")->get();
	$opts = array( 'view' => 'settings', );
	return view('settings.general', compact('homedata','opts'));
});

// register vendor
Route::get('/register/vendor', function(){
	return view('vendor.registervendor');
});
Route::post('/register/vendor', 'listings@saveVendor');	

// view vendor profile
Route::get('/vendor/{vid}', function($vid){
	$profile = DB::table('vendor')->where('vid', $vid)->get();
	return view('vendor.viewvendor', compact('profile'));
});
Route::get('/vendor/{vid}/edit', function($vid){
	$profile = DB::table('vendor')->where('vid', $vid)->get();
	return view('vendor.editvendor', compact('profile'));
});
Route::post('/vendor/{vid}/edit', 'listings@updatevendor');

// user base
Route::get('/register/user', function(){
	return view('users.nfpregister');
});
Route::post('/register/user', 'nfpusers@saveuser');	

Route::get('/users/{uid}', function($uid){
	$profile = DB::table('nfpusers')->where('uid', $uid)->get();
	$opts = array( 'view' => 'users', );
	return view('users.viewusers', compact('profile', 'opts'));
});

Route::post('/vendor/{vid}/edit', 'listings@updatevendor');

