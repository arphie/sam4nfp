@include('layout.header')


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			@include('layout.sidebar')


			<!-- Main content -->
			<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">@yield('title')</span></h4>
						<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>
					</div>
				</div>
				<!-- Content area -->
				<div class="content">

					@yield('content')

@include('layout.footer')

