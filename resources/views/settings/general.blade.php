@extends('dashboard.index')

@section('title')
	Site Settings
@endsection

@section('content')
{{ Form::open(array('url' => '/dashboard/settings', 'files'=>true)) }}
	{{ Form::token() }}
	@php
		$oldvalues = "";
		foreach ($homedata as $key => $value) { $oldvalues = $value->optvalue; }
		$allolddata = json_decode($oldvalues);
	@endphp
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>	
	<br />
	<div class="form-horizontal">
		<div class="panel panel-flat panel-collapsed">
			<div class="panel-heading">
				<h5 class="panel-title"><i class="icon-move-up"></i> Header Settings<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse" class="rotate-180"></a></li>
	            	</ul>
	        	</div>
			</div>
			<div class="panel-body">
			<div class="form-horizontal">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">Upload Site Logo</label>
							<div class="">
								 <div class="uploader">{{ Form::file('site_logo', ['class' => 'file-styled']) }}<span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-default legitRipple" style="user-select: none;">Choose File</span></div>
								{{ Form::hidden('site_logo_old', @$allolddata->site_logo) }}
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="sitelogo">
							<div class="innersite">
								<img src="{{ @$allolddata->site_logo }}">
							</div>
						</div>
					</div>
					<br />
				</div>
				<br />
				<br />
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">Upload Site Favicon</label>
							<div class="">
								 <div class="uploader">{{ Form::file('site_favicon', ['class' => 'file-styled']) }}<span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-default legitRipple" style="user-select: none;">Choose File</span></div>
								{{ Form::hidden('site_favicon_old', @$allolddata->site_favicon) }}
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="sitelogo">
							<div class="innersite">
								<img src="{{ @$allolddata->site_favicon }}">
							</div>
						</div>
					</div>
					<br />
				</div>
			</div>
			</div>
		</div>
		<div class="panel panel-flat panel-collapsed">
			<div class="panel-heading">
				<h5 class="panel-title"><i class="icon-insert-template"></i> Content Settings<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse" class="rotate-180"></a></li>
	            		{{-- <li><a data-action="reload"></a></li>
	            		<li><a data-action="close"></a></li> --}}
	            	</ul>
	        	</div>
			</div>
			<div class="panel-body">
				this is the body
			</div>
		</div>
		<div class="panel panel-flat panel-collapsed">
			<div class="panel-heading">
				<h5 class="panel-title"><i class="icon-move-down"></i> Footer Settings<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse" class="rotate-180"></a></li>
	            		{{-- <li><a data-action="reload"></a></li>
	            		<li><a data-action="close"></a></li> --}}
	            	</ul>
	        	</div>
			</div>
			<div class="panel-body">
				<div class="panel-body">
					<div class="form-group">
						<label class="control-label col-lg-2">Footer Copy right</label>
						<div class="col-lg-10">
							{{ Form::textarea('footer_copy_right', @$allolddata->footer_copy_right, ['class' => 'form-control']) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-flat panel-collapsed">
			<div class="panel-heading">
				<h5 class="panel-title"><i class="icon-gear"></i> Social Media<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse" class="rotate-180"></a></li>
	            		{{-- <li><a data-action="reload"></a></li>
	            		<li><a data-action="close"></a></li> --}}
	            	</ul>
	        	</div>
			</div>
			<div class="panel-body">
				<div class="panel-body">
					<div class="form-group">
						<label class="control-label col-lg-2">Facebook</label>
						<div class="col-lg-10">
							<div class="form-group has-feedback has-feedback-left">
								{{ Form::text('social_facebook', @$allolddata->social_facebook, ['class' => 'form-control input-xlg']) }}
								<div class="form-control-feedback">
									<i class="icon-facebook"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-2">Twitter</label>
						<div class="col-lg-10">
							<div class="form-group has-feedback has-feedback-left">
								{{ Form::text('social_twitter', @$allolddata->social_twitter, ['class' => 'form-control input-xlg']) }}
								<div class="form-control-feedback">
									<i class="icon-twitter"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-2">Youtube</label>
						<div class="col-lg-10">
							<div class="form-group has-feedback has-feedback-left">
								{{ Form::text('social_youtube', @$allolddata->social_youtube, ['class' => 'form-control input-xlg']) }}
								<div class="form-control-feedback">
									<i class="icon-youtube"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>
{{ Form::close() }}
@endsection