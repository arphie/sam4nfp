@extends('layout.template')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	
	@if (isset($alldata))
		thank you for registering
	@else
		{{ Form::open(array('url' => '/register/user', 'files'=>true)) }}
		{{ Form::token() }}
			<p>First Name: {{ Form::text('ufname') }}</p>
			<p>Last Name: {{ Form::text('ulname') }}</p>
			<p>Not for profit name: {{ Form::text('uorgname') }}</p>

			<p>Email: {{ Form::email('uemail', $value = null, $attributes = array()) }}</p>
			<p>Password: {{ Form::password('password') }}</p>
			<p>Condfirm Password: {{ Form::password('password_confirmation') }}</p>
			
			<p>
				{{ Form::file('verifydocu', ['class' => 'file-styled', 'required' => 'required']) }}
			</p>
			<div class="text-right">
				<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
			</div>
		{{ Form::close() }}
	@endif

@endsection