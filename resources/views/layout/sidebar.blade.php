<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					{{-- <div class="sidebar-user-material">
						<div class="category-content">
							<div class="sidebar-user-material-content">
								<a href="#"><img src="assets/images/placeholder.jpg" class="img-circle img-responsive" alt=""></a>
								<h6>John Peter Doe</h6>
								<span class="text-size-small">Santa Ana, CA</span>
							</div>
														
							<div class="sidebar-user-material-menu">
								<a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
							</div>
						</div>
						
						<div class="navigation-wrapper collapse" id="user-nav">
							<ul class="navigation">
								<li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
								<li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>
								<li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
								<li class="divider"></li>
								<li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
								<li><a href="#"><i class="icon-switch2"></i> <span>Logout</span></a></li>
							</ul>
						</div>
					</div> --}}
					<!-- /user menu -->
					@php
						$viewval = @$opts['view'];
					@endphp


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Site Content</span> <i class="icon-menu" title="Main pages"></i></li>
								<li ><a href="/dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li @if (@$viewval == 'pages') class="active" @endif >
									<a href="#"><i class="icon-stack2"></i> <span>Pages</span></a>
									<ul>
										<li><a href="/dashboard/pages/home"><i class="icon-pen2"></i> Home</a></li>
										<li><a href="/dashboard/pages/about"><i class="icon-pen2"></i> About</a></li>
										<li><a href="/dashboard/pages/pricing"><i class="icon-pen2"></i> Pricing</a></li>
										<li><a href="/dashboard/pages/contact"><i class="icon-pen2"></i> Contact</a></li>
										<li><a href="/dashboard/pages/privacy"><i class="icon-pen2"></i> Privacy Policy</a></li>
										<li><a href="/dashboard/pages/terms"><i class="icon-pen2"></i> Terms & Conditions</a></li>
									</ul>
								</li>
								<li @if (@$viewval == 'settings') class="active" @endif ><a href="/dashboard/settings"><i class="icon-gear"></i> <span>Settings</span></a></li>
								<li @if (@$viewval == 'vendors') class="active" @endif><a href="/dashboard/vendors"><i class="icon-reading"></i> <span>Vendors</span></a></li>
								<li @if (@$viewval == 'users') class="active" @endif><a href="/dashboard/users"><i class="icon-heart5"></i> <span>Users</span></a></li>
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->