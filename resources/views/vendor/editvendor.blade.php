@extends('layout.template')

@section('content')
	
	edit vendor profile
	@php
		$oldvalues = "";
		foreach ($profile as $key => $value) { $oldvalues = $value; }
	@endphp
	<pre>
		{{ print_r($oldvalues) }}
	</pre>
	{{ Form::open(array('url' => '/vendor/'.$oldvalues->vid.'/edit' , 'files'=>true)) }}
		{{ Form::token() }}
			<p>Email: {{ Form::email('uemail', $oldvalues->uemail , $attributes = array()) }}</p>
			{{-- <p>Password: {{ Form::password('upass') }}</p> --}}
			<p>First Name: {{ Form::text('fname', $oldvalues->fname) }}</p>
			<p>Last Name: {{ Form::text('lname', $oldvalues->lname) }}</p>
			<p>Phone Number: {{ Form::text('phonenum', $oldvalues->phonenum) }}</p>
			<p>Company Number: {{ Form::text('companyname', $oldvalues->companyname) }}</p>
			<p>Web URL: {{ Form::text('websiteurl', $oldvalues->websiteurl) }}</p>
			<p>Address: {{ Form::text('address', $oldvalues->uaddress) }}</p>
			@php
				$doflisting = ($oldvalues->getmostoflisting != "" ? json_decode($oldvalues->getmostoflisting) : array());
			@endphp
			<p>
				Get the most out of your Listing: <br>
				{{ Form::checkbox('get_most_listing[]', 'learn_generating_leads', (in_array("learn_generating_leads", $doflisting) ? true : false )) }} I want to Learn about generating leads from my listing<br>
				{{ Form::checkbox('get_most_listing[]', 'use_PPC', (in_array("use_PPC", $doflisting) ? true : false )) }} I use other PPC advertising channels<br>
				{{ Form::checkbox('get_most_listing[]', 'schedule_free_review', (in_array("schedule_free_review", $doflisting) ? true : false )) }} I would like to schedule a free website review<br>
			</p>
			<div class="text-right">
				<p>{{ Form::hidden('vid', $oldvalues->vid) }}</p>
				<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
			</div>
		{{ Form::close() }}
@endsection