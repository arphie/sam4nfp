@extends('layout.template')

@section('content')
	
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	@if (isset($alldata))
		thank you for registering
	@else
		{{ Form::open(array('url' => '/register/vendor', 'files'=>true)) }}
		{{ Form::token() }}
			<p>Email: {{ Form::email('uemail', $value = null, $attributes = array()) }}</p>
			<p>Password: {{ Form::password('upass') }}</p>
			<p>Confirm Password: {{ Form::password('upass_confirmation') }}</p>
			<p>First Name: {{ Form::text('fname') }}</p>
			<p>Last Name: {{ Form::text('lname') }}</p>
			<p>Phone Number: {{ Form::text('phonenum') }}</p>
			<p>Company Number: {{ Form::text('companyname') }}</p>
			<p>Web URL: {{ Form::text('websiteurl') }}</p>
			<p>Address: {{ Form::text('address') }}</p>
			<p>
				Get the most out of your Listing: <br>
				{{ Form::checkbox('get_most_listing[]', 'learn_generating_leads') }} I want to Learn about generating leads from my listing<br>
				{{ Form::checkbox('get_most_listing[]', 'use_PPC') }} I use other PPC advertising channels<br>
				{{ Form::checkbox('get_most_listing[]', 'schedule_free_review') }} I would like to schedule a free website review<br>
			</p>
			
			<div class="text-right">
				<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
			</div>
		{{ Form::close() }}
	@endif
	
@endsection