@extends('dashboard.index')

@section('title')
	Home Page Template
@endsection

@section('content')
{{ Form::open(array('url' => '/dashboard/pages/home', 'files'=>true)) }}
{{ Form::token() }}
	@php
		$oldvalues = "";
		foreach ($homedata as $key => $value) { $oldvalues = $value->optvalue; }
		$allolddata = json_decode($oldvalues);
	@endphp
	@if (@$opts['fstatus'] == 'success')
		<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			<span class="text-semibold">Well done!</span> You successfully updated the page
	    </div>
	@endif
	
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>	
	<br />
	<div class="panel panel-flat">
		<div class="panel-heading"></div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">

						<div class="form-group">
							<label class="control-label">Home Banner Title</label>
							<div class="">
								{{ Form::text('home_banner_title', @$allolddata->home_banner_title, ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label=">Home Banner Sub-title</label>
							<div class="">
								{{ Form::text('home_banner_sub_title', @$allolddata->home_banner_sub_title, ['class' => 'form-control']) }}
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label">Upload Background Image</label>
							<div class="">
								 <div class="uploader">{{ Form::file('home_banner_background', ['class' => 'file-styled']) }}<span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-default legitRipple" style="user-select: none;">Choose File</span></div>
							</div>
						</div>
						@if (@$allolddata->home_banner_background != "")
							<div class="featured_image homebase">
								<div class="baseinner">
									<img src="{{ @$allolddata->home_banner_background }} ">
									{{ Form::hidden('home_banner_background_backup', @$allolddata->home_banner_background) }}
								</div>
							</div>
						@endif

					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>
{{ Form::close() }}
@endsection