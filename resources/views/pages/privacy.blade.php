@extends('dashboard.index')

@section('title')
	Privacy Page Template
@endsection

@section('content')
{{ Form::open(array('url' => '/dashboard/pages/privacy', 'files'=>true)) }}
	{{ Form::token() }}
	@php
		$oldvalues = "";
		foreach ($homedata as $key => $value) { $oldvalues = $value->optvalue; }
		$allolddata = json_decode($oldvalues);
	@endphp
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>
	<br />
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Primary Content<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">

						<div class="form-group">
							<label class="control-label">Primary Title</label>
							<div class="">
								{{ Form::text('privacy_primary_title', @$allolddata->privacy_primary_title, ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label=">Primary Content</label>
							<div class="">
								{{ Form::text('privacy_primary_content', @$allolddata->privacy_primary_content, ['class' => 'form-control']) }}
							</div>
						</div>

					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Secondary Content<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">

						<div class="form-group">
							<label class="control-label">Secondary Title</label>
							<div class="">
								{{ Form::text('privacy_secondary_left_title', @$allolddata->privacy_secondary_left_title, ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label=">Secondary Left Content</label>
								<div class="">
									{{ Form::textarea('privacy_secondary_left_content', @$allolddata->privacy_secondary_left_content, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label=">Secondary Right Content</label>
								<div class="">
									{{ Form::textarea('privacy_secondary_right_content', @$allolddata->privacy_secondary_right_content, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
						<br class="clear">
						<div class="form-group">
								<label class="control-label=">Secondary Center Content</label>
								<div class="">
									{{ Form::textarea('privacy_secondary_center_content', @$allolddata->privacy_secondary_center_content, ['class' => 'form-control']) }}
								</div>
							</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>
{{ Form::close() }}
@endsection