@extends('dashboard.index')

@section('title')
	Vendors
@endsection

@section('content')
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">List of Vendors<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Company</th>
					<th>Status</th>
					<th class="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($listofvendors as $element)
					<tr>
						<td>{{ $element->fname }}</td>
						<td>{{ $element->lname }}</td>
						<td>{{ $element->uemail }}</td>
						<td><a href="#">{{ $element->companyname }}</a></td>
						<td>
							@if ($element->isActive == 0)
								<span class="label label-default">Inactive</span>
							@else
								<span class="label label-success">Active</span>
							@endif
						</td>
						<td class="text-center">
							<ul class="icons-list">
								<li class="dropdown">	
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="icon-menu9"></i>
									</a>

									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											@if ($element->isActive == 0)
												<a href="/dashboard/vendors/{{ $element->vid }}/toactive"><i class="icon-user-check"></i> Activate</a>
											@else
												<a href="/dashboard/vendors/{{ $element->vid }}/toinactive"><i class="icon-user-cancel"></i> Set Inactive</a>
											@endif
										</li>
										<li><a href="/vendor/{{ $element->vid }}" target="_blank"><i class="icon-man-woman"></i> View Profile</a></li>
										{{-- <li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li> --}}
									</ul>
								</li>
							</ul>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection