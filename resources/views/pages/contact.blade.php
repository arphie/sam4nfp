@extends('dashboard.index')

@section('title')
	Contact Page Template
@endsection

@section('content')
{{ Form::open(array('url' => '/dashboard/pages/contact', 'files'=>true)) }}
	{{ Form::token() }}
	@php
		$oldvalues = "";
		foreach ($homedata as $key => $value) { $oldvalues = $value->optvalue; }
		$allolddata = json_decode($oldvalues);
	@endphp
<div class="text-right">
	<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
</div>
<br />
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Top Section<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">

						<div class="form-group">
							<label class="control-label">Contact Banner Title</label>
							<div class="">
								{{ Form::text('contact_banner_title', @$allolddata->contact_banner_title, ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label=">Contact Banner Sub-title</label>
							<div class="">
								{{ Form::text('contact_banner_sub_title', @$allolddata->contact_banner_sub_title, ['class' => 'form-control']) }}
							</div>
						</div>

					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Form Section<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">

						FORM DATA HERE

					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-flat panel-collapsed">
			<div class="panel-heading">
				<h5 class="panel-title">Left Columned Section<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            	</ul>
	        	</div>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					<fieldset class="content-group">
						<div class="innerforms">

							<div class="form-group">
								<label class="control-label">Title</label>
								<div class="">
									{{ Form::text('contact_left_col_title', @$allolddata->contact_left_col_title, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Description</label>
								<div class="">
									{{ Form::textarea('contact_left_col_desc', @$allolddata->contact_left_col_desc, ['class' => 'form-control']) }}
								</div>
							</div>

						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
			<div class="panel panel-flat panel-collapsed">
			<div class="panel-heading">
				<h5 class="panel-title">Right Columned Section<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            	</ul>
	        	</div>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					<fieldset class="content-group">
						<div class="innerforms">

							<div class="form-group">
								<label class="control-label">Title</label>
								<div class="">
									{{ Form::text('contact_right_col_title', @$allolddata->contact_right_col_title, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Description</label>
								<div class="">
									{{ Form::textarea('contact_right_col_desc', @$allolddata->contact_right_col_desc, ['class' => 'form-control']) }}
								</div>
							</div>

						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	</div>
<div class="text-right">
	<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
</div>
{{ Form::close() }}
@endsection