@extends('dashboard.index')

@section('title')
	About Us Page Template
@endsection

@section('content')
{{ Form::open(array('url' => '/dashboard/pages/about', 'files'=>true)) }}
	{{ Form::token() }}
	
	@php
		$oldvalues = "";
		foreach ($homedata as $key => $value) { $oldvalues = $value->optvalue; }
		$allolddata = json_decode($oldvalues);
	@endphp

	@if (@$opts['fstatus'] == 'success')
		<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			<span class="text-semibold">Well done!</span> You successfully updated the page
	    </div>
	@endif

	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>
	<br /> 
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Top Section<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">

						<div class="form-group">
							<label class="control-label">About Us Banner Title</label>
							<div class="">
								{{ Form::text('about_banner_title', @$allolddata->about_banner_title, ['class' => 'form-control']) }}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label=">About Us Banner Sub-title</label>
							<div class="">
								{{ Form::text('about_banner_sub_title', @$allolddata->about_banner_sub_title, ['class' => 'form-control']) }}
							</div>
						</div>

					</div>
				</fieldset>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-flat panel-collapsed">
				<div class="panel-heading">
					<h5 class="panel-title">CTA Section 1<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<fieldset class="content-group">
							<div class="innerforms">
									<div class="innercta">
										@if (@$allolddata->about_cta1_image != "")
											<div class="featured_image">
												<div class="baseinner">
													<img src="{{ @$allolddata->about_cta1_image }} ">
												</div>
											</div>
										@endif
										<div class="form-group">
											<label class="control-label">Upload CTA image</label>
											<div class="">
												<div class="uploader">{{ Form::file('about_cta1_image', ['class' => 'file-styled']) }}<span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-default legitRipple" style="user-select: none;">Choose File</span></div>
											</div>
											{{ Form::hidden('about_cta1_image_backup', @$allolddata->about_cta1_image) }}
										</div>
										<div class="form-group">
											<label class="control-label">CTA 1 Title</label>
											<div class="">
												{{ Form::text('about_cta1_title', @$allolddata->about_cta1_title, ['class' => 'form-control']) }}
											</div>
										</div>
										<div class="form-group">
											<label class="control-label">CTA 1 Description</label>
											<div class="">
												{{ Form::textarea('about_cta1_description', @$allolddata->about_cta1_description, ['class' => 'form-control']) }}
											</div>
										</div>
									</div>
								<br class="clear">
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-flat panel-collapsed">
				<div class="panel-heading">
					<h5 class="panel-title">CTA Section 2<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<fieldset class="content-group">
							<div class="innerforms">
									<div class="innercta">
										@if (@$allolddata->about_cta1_image != "")
											<div class="featured_image">
												<div class="baseinner">
													<img src="{{ @$allolddata->about_cta2_image }} ">
												</div>
											</div>
										@endif
										<div class="form-group">
											<label class="control-label">Upload CTA image</label>
											<div class="">
												<div class="uploader">{{ Form::file('about_cta2_image', ['class' => 'file-styled']) }}<span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-default legitRipple" style="user-select: none;">Choose File</span></div>
											</div>
											{{ Form::hidden('about_cta2_image_backup', @$allolddata->about_cta2_image) }}
										</div>
										<div class="form-group">
											<label class="control-label">CTA 2 Title</label>
											<div class="">
												{{ Form::text('about_cta2_title', @$allolddata->about_cta2_title, ['class' => 'form-control']) }}
											</div>
										</div>
										<div class="form-group">
											<label class="control-label">CTA 2 Description</label>
											<div class="">
												{{ Form::textarea('about_cta2_description', @$allolddata->about_cta2_description, ['class' => 'form-control']) }}
											</div>
										</div>
									</div>
								<br class="clear">
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-flat panel-collapsed">
				<div class="panel-heading">
					<h5 class="panel-title">CTA Section 3<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
		            		<li><a data-action="collapse"></a></li>
		            	</ul>
		        	</div>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<fieldset class="content-group">
							<div class="innerforms">
									<div class="innercta">
										@if (@$allolddata->about_cta1_image != "")
											<div class="featured_image">
												<div class="baseinner">
													<img src="{{ @$allolddata->about_cta3_image }} ">
												</div>
											</div>
										@endif
										<div class="form-group">
											<label class="control-label">Upload CTA image</label>
											<div class="">
												<div class="uploader">{{ Form::file('about_cta3_image', ['class' => 'file-styled']) }}<span class="filename" style="user-select: none;">No file selected</span><span class="action btn btn-default legitRipple" style="user-select: none;">Choose File</span></div>
											</div>
											{{ Form::hidden('about_cta3_image_backup', @$allolddata->about_cta3_image) }}
										</div>
										<div class="form-group">
											<label class="control-label">CTA 3 Title</label>
											<div class="">
												{{ Form::text('about_cta3_title', @$allolddata->about_cta3_title, ['class' => 'form-control']) }}
											</div>
										</div>
										<div class="form-group">
											<label class="control-label">CTA 3 Description</label>
											<div class="">
												{{ Form::textarea('about_cta3_description', @$allolddata->about_cta3_description, ['class' => 'form-control']) }}
											</div>
										</div>
									</div>
								<br class="clear">
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- <br class="clear"> --}}
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">CTA Goto Link<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">GoTo link Text</label>
								<div class="">
									{{ Form::text('about_goto_link', @$allolddata->about_goto_link, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label=">GoTo link Url</label>
								<div class="">
									{{ Form::text('about_goto_url', @$allolddata->about_goto_url, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
						<br clear="clear">
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Middle Content<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Featured Text</label>
								<div class="">
									{{ Form::text('about_middle_text', @$allolddata->about_middle_text, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Featured Description</label>
								<div class="">
									{{ Form::textarea('about_middle_description', @$allolddata->about_middle_description, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
						<br clear="clear">
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel panel-flat panel-collapsed">
		<div class="panel-heading">
			<h5 class="panel-title">Bottom Section<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<div class="form-horizontal">
				<fieldset class="content-group">
					<div class="innerforms">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Featured Text</label>
								<div class="">
									{{ Form::text('about_bottom_text', @$allolddata->about_bottom_text, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Goto Text</label>
								<div class="">
									{{ Form::text('about_bottom_goto_text', @$allolddata->about_bottom_goto_text, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Goto Link</label>
								<div class="">
									{{ Form::text('about_bottom_goto_link', @$allolddata->about_bottom_goto_link, ['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Video Link</label>
								<div class="">
									{{ Form::text('about_bottom_video_link', @$allolddata->about_bottom_video_link, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Featured Description</label>
								<div class="">
									{{ Form::textarea('about_bottom_description', '', ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
						<br clear="clear">
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="text-right">
		<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-database-insert position-right"></i></button>
	</div>
{{ Form::close() }}
@endsection