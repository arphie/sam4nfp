<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nfpusersmodel extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'uid';
    protected $table = 'nfpusers';
	
	protected $fillable = [
		'ufname',
		'ulname',
		'uorgname',
		'uemail',
		'upass',
		'verifydocu',
	];

	public $timestamps = false;
}
