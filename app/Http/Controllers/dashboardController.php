<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\r;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class dashboardController extends Controller
{

    /**
     * Save Pages in Dashboard
     *
     * $saveto -> to be used as a reference in the database
     * 
     */
    public function saveTest(Request $request, $saveto)
    {
        // get all post value from form
        $pagedata = $request->all();

        // remove form token before saving the data in DB
        unset($pagedata['_token']);

        echo "<pre>";
            print_r($pagedata);
        echo "</pre>";

        echo public_path('images')."/".$request->home_banner_background->getClientOriginalName();

        // $request->home_banner_background->move(public_path('images'), $request->home_banner_background->getClientOriginalName());

    }

    public function savePage(Request $request, $saveto)
    {
        // get all post value from form
        $pagedata = $request->all();

        // upload image
        if($saveto == 'home'){ // images for home page
            if (isset($pagedata['home_banner_background']) && $pagedata['home_banner_background'] != "") {
                $pagedata['home_banner_background'] = "/images/".$request->home_banner_background->getClientOriginalName();
                $request->home_banner_background->move(public_path('images'), $request->home_banner_background->getClientOriginalName());
            } else { $pagedata['home_banner_background'] = $pagedata['home_banner_background_backup']; }
        } elseif($saveto == 'about'){ // images for About page
            if (isset($pagedata['about_cta1_image']) && $pagedata['about_cta1_image'] != "") {
                $pagedata['about_cta1_image'] = "/images/".$request->about_cta1_image->getClientOriginalName();
                $request->about_cta1_image->move(public_path('images'), $request->about_cta1_image->getClientOriginalName());
            } else { $pagedata['about_cta1_image'] = $pagedata['about_cta1_image_backup']; }

            if (isset($pagedata['about_cta2_image']) && $pagedata['about_cta2_image'] != "") {
                $pagedata['about_cta2_image'] = "/images/".$request->about_cta2_image->getClientOriginalName();
                $request->about_cta2_image->move(public_path('images'), $request->about_cta2_image->getClientOriginalName());
            } else { $pagedata['about_cta2_image'] = $pagedata['about_cta2_image_backup']; }

            if (isset($pagedata['about_cta3_image']) && $pagedata['about_cta3_image'] != "") {
                $pagedata['about_cta3_image'] = "/images/".$request->about_cta3_image->getClientOriginalName();
                $request->about_cta3_image->move(public_path('images'), $request->about_cta3_image->getClientOriginalName());
            } else { $pagedata['about_cta3_image'] = $pagedata['about_cta3_image_backup']; }
        }

        // remove form token and other temporary fields before saving the data in DB
        unset($pagedata['_token'], $pagedata['home_banner_background_backup'], $pagedata['about_cta1_image_backup'], $pagedata['about_cta2_image_backup'], $pagedata['about_cta3_image_backup']);

        // save data on DB for all pages section only
        DB::table('settings')->where('optname', $saveto."page")->update(['optvalue' => json_encode($pagedata)]);

        //get new values of the page to load on dashboard
        $homedata = DB::table('settings')->where('optname', $saveto."page")->get();

        // insert additional parameters
        $opts = ['view' => 'pages', 'fstatus' => 'success'];

        return view('pages.'.$saveto, compact('homedata', 'opts'));
    }

    public function saveSettings(Request $request)
    {
        $generaldata = $request->all();

        //upload image 
        if (isset($generaldata['site_logo']) && $generaldata['site_logo'] != "") {
            $generaldata['site_logo'] = "/images/".$request->site_logo->getClientOriginalName();
            $request->site_logo->move(public_path('images'), $request->site_logo->getClientOriginalName());
        } else { $generaldata['site_logo'] = $generaldata['site_logo_old']; }

        if (isset($generaldata['site_favicon']) && $generaldata['site_favicon'] != "") {
            $generaldata['site_favicon'] = "/images/".$request->site_favicon->getClientOriginalName();
            $request->site_favicon->move(public_path('images'), $request->site_favicon->getClientOriginalName());
        } else { $generaldata['site_favicon'] = $generaldata['site_favicon_old']; }

        // remove form token and other temporary fields before saving the data in DB
        unset($generaldata['_token'], $generaldata['site_logo_old'], $generaldata['site_favicon_old']);

        // save data on DB for Settings Page
        DB::table('settings')->where('optname', "settings")->update(['optvalue' => json_encode($generaldata)]);

        $homedata = DB::table('settings')->where('optname', "settings")->get();
        $opts = array( 'view' => 'settings', );
        return view('settings.general', compact('homedata','opts'));

    }

    public function changesVendorStatus($vid, $action)
    {

        if ($action == "toactive") {
            DB::table('vendor')->where('vid', $vid )->update(['isActive' => 1]);
        } else {
            DB::table('vendor')->where('vid', $vid )->update(['isActive' => 0]);
        }
       return redirect('/dashboard/vendors')->with('status', 'Profile updated!');
    }
}
