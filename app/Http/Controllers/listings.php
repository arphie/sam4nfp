<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\listingmodel;
use Illuminate\Http\Request;

class listings extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\listingmodel  $listingmodel
     * @return \Illuminate\Http\Response
     */
    public function show(listingmodel $listingmodel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\listingmodel  $listingmodel
     * @return \Illuminate\Http\Response
     */
    public function edit(listingmodel $listingmodel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\listingmodel  $listingmodel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, listingmodel $listingmodel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\listingmodel  $listingmodel
     * @return \Illuminate\Http\Response
     */
    public function destroy(listingmodel $listingmodel)
    {
        //
    }

    public function savevendor(Request $request)
    {
        $alldata = $request->all();

        $this->validate($request, [
            'uemail' => 'required|string|email|max:255|unique:vendor',
            'upass' => 'required|string|min:6|confirmed',
            'upass_confirmation' => 'required|min:6:pass',
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'phonenum' => 'required|string|max:255',
            'companyname' => 'required|string|max:255',
            'websiteurl' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ]);

        $save = new listingmodel;
        $save->uemail           = $alldata['uemail'];
        $save->upass            = bcrypt($alldata['upass']);
        $save->fname            = $alldata['fname'];
        $save->lname            = $alldata['lname'];
        $save->phonenum         = $alldata['phonenum'];
        $save->companyname      = $alldata['companyname'];
        $save->websiteurl       = $alldata['websiteurl'];
        $save->uaddress         = $alldata['address'];
        $save->getmostoflisting = json_encode($alldata['get_most_listing']);

        // echo "<pre>";
        //     print_r($save);
        // echo "</pre>";

        $save->save();

        return view('vendor.registervendor', compact('alldata'));
    }

    public function updatevendor(Request $request)
    {
        $alldata = $request->all();

        DB::table('vendor')->where('vid', $alldata['vid'])->update([
            'uemail' => $alldata['uemail'],
            'fname' => $alldata['fname'],
            'lname' => $alldata['lname'],
            'phonenum' => $alldata['phonenum'],
            'companyname' => $alldata['companyname'],
            'websiteurl' => $alldata['websiteurl'],
            'uaddress' => $alldata['address'],
            'getmostoflisting' => json_encode($alldata['get_most_listing']),
            ]);
        return redirect('/vendor/'.$alldata['vid'])->with('status', 'Profile updated!');
    }
}
