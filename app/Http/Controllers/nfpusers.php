<?php

namespace App\Http\Controllers;

use App\nfpusersmodel;
use Illuminate\Http\Request;

class nfpusers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\nfpusersmodel  $nfpusersmodel
     * @return \Illuminate\Http\Response
     */
    public function show(nfpusersmodel $nfpusersmodel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\nfpusersmodel  $nfpusersmodel
     * @return \Illuminate\Http\Response
     */
    public function edit(nfpusersmodel $nfpusersmodel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\nfpusersmodel  $nfpusersmodel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, nfpusersmodel $nfpusersmodel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\nfpusersmodel  $nfpusersmodel
     * @return \Illuminate\Http\Response
     */
    public function destroy(nfpusersmodel $nfpusersmodel)
    {
        //
    }

    public function saveuser(Request $request)
    {
        

        $this->validate($request, [
            'ufname' => 'required|string|max:255',
            'ulname' => 'required|string|max:255',
            'uorgname' => 'required|string|max:255',
            'uemail' => 'required|string|email|max:255|unique:nfpusers',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|min:6:pass',
        ]);

        $alldata = $request->all();

        $save = new nfpusersmodel;
        $save['ufname'] = $alldata['ufname'];
        $save['ulname'] = $alldata['ulname'];
        $save['uorgname'] = $alldata['uorgname'];
        $save['uemail'] = $alldata['uemail'];
        $save['upass'] = bcrypt($alldata['password']);

        $save['verifydocu'] = "/images/".$request->verifydocu->getClientOriginalName();
        $request->verifydocu->move(public_path('images'), $request->verifydocu->getClientOriginalName());
        
        $save->save();

        return view('frontend.nfpregister', compact('alldata'));
    }
}
