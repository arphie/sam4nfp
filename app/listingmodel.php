<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class listingmodel extends Model
{
	
	protected $connection = 'mysql';
    protected $primaryKey = 'vid';
    protected $table = 'vendor';
	
	protected $fillable = [
		'uemail',
		'upass',
		'fname',
		'lname',
		'phonenum',
		'companyname',
		'websiteurl',
		'uaddress',
		'getmostoflisting',
	];

	public $timestamps = false;

}
